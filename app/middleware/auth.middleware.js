'use strict'
const jwt = require('jsonwebtoken')
const config = require('config')

/**
 * Use the middleware for any routes (except auth.routes.js)
 * */
module.exports = (req, res, next) => {

    /** break if test request **/
    if (req.method === 'OPTIONS')
        return next()

    try {
        /** parse token from authorization header **/
        const token = req.headers.authorization.split(' ')[1]

        /** break if token non-valid **/
        if (!token)
            return res.status(401).json('no authorization')

        /** set user jwt object  to request **/
        req.user = jwt.verify(token, config.get('jwtSecret'))
        /** apply next handler **/
        next()
    }
    catch (e) {
        res.status(401).json('no authorization')
    }
}